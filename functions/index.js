const functions = require("firebase-functions");
const firebase = require('firebase');
const express = require('express');
const admin = require('firebase-admin');

var firebaseConfig = {
  apiKey: "AIzaSyDfEDDDxwPlsA4K28CkfPdWTdRGOV0H4Y0",
  authDomain: "pranali-demos.firebaseapp.com",
  databaseURL: "https://pranali-demos.firebaseio.com/",
  projectId: "pranali-demos",
  storageBucket: "gs://pranali-demos.appspot.com",
};

admin.initializeApp();

const bodyparser = require("body-parser");
const userRoutes = require("./src/routes/router");
const app = express();

app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());
app.use(bodyparser.json({ type: 'application/*+json' }))
app.use('/images', express.static('./images'));
// parse some custom thing into a Buffer
app.use(bodyparser.raw({ type: 'application/vnd.custom-type' }))
// parse an HTML body into a string
app.use(bodyparser.text({ type: 'text/html' }))
app.use(userRoutes);
app.use(express.json());

exports.texastock = functions.https.onRequest(app);
exports.helloWorld = functions.https.onRequest((request, response) => {
  functions.logger.info("Hello logs!", {structuredData: true});
  response.send("Hello from Firebase!");
});