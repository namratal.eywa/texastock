//const jwt = require('jsonwebtoken');
//const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db = admin.firestore();
const fieldValue = admin.firestore.FieldValue;

// Add search history
exports.searchProduct = async (req, res) => {
  try {
    const {
      ownerKey,
      ownerName,
      productKey,
      productName
    } = req.body;
    if(!productKey) throw new Error('Product Key is required');
    const snapshot = await db.collection('products').doc(productKey).get();
    const product = await snapshot.data();
    var found = parseInt(product.searchCount);
    if(!snapshot.exists){
      found = parseInt(product.searchCount);
      throw new Error('Product doesent exists');
    }else{
      found = found + 1;
      console.log("Product Found..", productKey);
    }
    const timestamp = fieldValue.serverTimestamp();
    const userRef = await db.collection('user').doc(req.body.ownerKey).get();
    const result = await userRef.data();
    const search = db.collection('user.searchHistory').doc();
    const id3 = search.id;
    
    const data2 = {
        key: id3,
        ownerKey: result.key,
        ownerName: result.displayName,
        productKey,
        productName,
        productDescription:product.description,
        imagePath:product.imagePath,
        isActive:true,
        createdAt: timestamp,
        updatedAt: timestamp
    }
    const data1 = {
      
    }
    const data = {
      searchCount: found,
    }
    search.set(data2, { merge:true});
    const search_history = db.collection('user').doc(productKey).collection('searchHistory').doc(id3);
    search_history.set(data2, { mearge: true });
    const product_search = db.collection('products').doc(productKey);
    product_search.set(data, {merge:true});

    res.json({
      id: snapshot.id,
      message: "User Search History Added Successfully"
    });
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
}
