const jwt = require('jsonwebtoken');
const admin = require("firebase-admin");
const db = admin.firestore();
const fieldValue = admin.firestore.FieldValue;

// Add new user Inventory
exports.addCategories = (req, res) => {
  try {
    const {
      name,
      description,
      subcategories,
      subcategoriesName,
      subcategoriesDescription,
      subcategoriesItems
    } = req.body;
    const timestamp = fieldValue.serverTimestamp();
    const data = {
      name,
      description,
      subcategories,
      subcategoriesName,
      subcategoriesDescription,
      subcategoriesItems,
      isActive:true,
      createdAt: timestamp,
      updatedAt:timestamp
    }
    const products = db.collection('categories').add(data);
    res.json({
      message: "Categories Added Successfully"
    });
  } catch (error) {
    res.status(500).send(error);
  }
}

// Update User Inventory
exports.updateCategories = (req, res) => {
  try {
    const userId = req.params.id;
    const {
      name,
      description,
      subcategories,
      subcategoriesName,
      subcategoriesDescription,
      subcategoriesItems   
    } = req.body;
    const timestamp = fieldValue.serverTimestamp();
    const data = {
      key: userId,
      name,
      description,
      subcategories,
      subcategoriesName,
      subcategoriesDescription,
      subcategoriesItems,
      createdAt:timestamp,   
      updatedAt: timestamp
    }
    const products = db.collection('categories').doc(userId).set(data, { merge: true});
    res.json({
      message: "Categories Updated Successfully"
    })
  } catch (error) {
    res.status(500).send(error);
  }
}
