const admin = require("firebase-admin");
const firebase = require('firebase');
const db = admin.firestore();
const fieldValue = admin.firestore.FieldValue;
const nodemailer = require('nodemailer');
const uuid = require("uuid-v4");
const multer = require("multer");
const path = require("path");

//upload image 
exports.upload_image = async (req, res) => {
    try {
        var bucket = admin.storage().bucket('gs://pranali-demos.appspot.com');
        var filename = "E:/texastock/functions/src/images/sample.png"
        await bucket.upload(filename, {
          metadata: {
            contentType:'image/jpeg',
            cacheControl: "max-age=31536000",
            public:true,
            metadata: {
              firebaseStorageDownloadTokens: uuid(),
            },
          },
        });
        console.log('File uploaded');
        res.status(200).send({message: "Image Upload Succssful"});
    } catch (error) {
        console.log(error);
        res.send("Image Not Uploaded");
    }
}
// Add new user Inventory
exports.addProducts = async(req, res) => {
  try {
    const {
      name,
      categoryKey,
      categoryName,
      subcategoryName,
      itemType,
      otherItemType,
      imagePath,
      description,
      material,
      color,
      stockQuantity,
      price,
      currency,
      ownerKey,
      searchCount,
      size,
      grade,
      unitOfMeasure,
      otherUnitOfMesure
    } = req.body;
    const timestamp = fieldValue.serverTimestamp();
    const time = new Date().toLocaleString(); 
    const active = true;
    const products = db.collection('products').doc();
    const inventory = db.collection('user.inventory').doc();
    const wishlist =db.collection('user.wishlist').doc();
    const userRef = await db.collection('user').doc(req.body.ownerKey).get();
    const result = await userRef.data();
    const id1 = inventory.id;
    const id2 = products.id;
    const id4 = wishlist.id;
    console.log(req.body);
    console.log(result.key, result.displayName, result.email);

    const data3 = {
      key:id4,
      userKey:result.key,
      userName:result.displayName,
      productKey:id2,
      productName:name,
      productDescription:description,
      imagePath,
      requiredQuantity:stockQuantity,
      isActive:true,
      createdAt:timestamp,
      updatedAt:timestamp
    }

    const data1 = {
      ownerKey:result.key,
      ownerName:result.displayName,
      productKey: id2,
      productName: name,
      productDescription: description,
      imagePath,
      price,
      color,
      material,
      categoryName,
      subcategoryName,
      itemType,
      size,
      grade,
      unitOfMeasure,
      otherUnitOfMesure,
      isActive: true,
      key: id1,
      createdAt: timestamp,
      updatedAt: timestamp
    }
    const data = {
      name,
      key: id2,
      categoryKey,
      categoryName,
      subcategoryName,
      itemType,
      otherItemType,
      imagePath,
      description,
      material,
      color,
      stockQuantity,
      price,
      currency,
      ownerKey:result.key,
      ownerName:result.displayName,
      searchCount,
      size,
      grade,
      unitOfMeasure,
      otherUnitOfMesure,
      isActive: active,
      createdAt: timestamp,
      updatedAt: timestamp
    }

    products.set(data, { merge: true });
    //inventory.set(data1, { merge: true });
    //wishlist.set(data3, { merge:true});
    const user_inventory = db.collection('user').doc(id2).collection('inventory').doc(id1);
    const user_wishlist = db.collection('user').doc(id2).collection('wishlist').doc(id4);
    user_inventory.set(data1, { merge: true });
    user_wishlist.set(data3, { merge: true});

    res.json({
      message: "Products Added Successfully"
    });
    var transport = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      secure: false, // true for 587, false for other ports
      requireTLS: true,
      auth: {
        user: "a.06.namrata.lagshetti",
        pass: "Lagshetti#54321"
      }, tls: {
        rejectUnauthorized: false
      }
    });
    const jsonObj = `
    <h3> Product Details: </h3>
    <p> Name: ${req.body.name}</p>
    <p> Category: ${req.body.categoryKey}</p>
    <p> Category Name: ${req.body.categoryName}</p>
    <p> Subcategory Name: ${req.body.subcategoryName}</p>
    <p> Item Type : ${req.body.itemType}</p>
    <p> Other Item Type: ${req.body.otherItemType}</p>
    <p> Image Path: ${req.body.imagePath}</p>
    <p> Description: ${req.body.description}</p>
    <p> Material: ${req.body.material}</p>
    <p> color: ${req.body.color}</p>
    <p> Stock Quantity: ${req.body.stockQuantity}</p>
    <p> Price: ${req.body.price}</p>
    <p> Currency: ${req.body.currency}</p>
    <p> Owner Key: ${req.body.ownerKey}</p>
    <p> Owner Name: ${result.displayName}</p>
    <p> Search Count: ${req.body.searchCount}</p>
    <p> Is Active: ${active}</p>
    <p> Created At: ${time}</p>
    `;

    console.log(time);
    console.log(result);
    
    const mailOptions = {
      from: 'a.06.namrata.lagshetti@gmail.com',
      to: result.email,
      subject: 'New Product Added ' + id2,
      text: '',
      html: jsonObj
    };

    transport.sendMail(mailOptions, function (err, info) {
      if (err) {
        console.log(err);
        res.json({message:"Email Not Sent"});
      } else {
        console.log("Email Sent " + info.response);       
        res.json({message:"Email Sent"});
      }
    });
  } catch(error) {
    console.log(error);
    res.status(500).send(error);
  }
}

// Update User Inventory
exports.updateProducts = (req, res) => {
  try {
    const userId = req.params.id;
    const {
      name,
      categoryKey,
      categoryName,
      subcategoryName,
      itemType,
      otherItemType,
      imagePath,
      description,
      material,
      color,
      stockQuantity,
      price,
      currency,
      ownerKey,
      ownerName,
      searchCount,
      size,
      grade,
      unitOfMeasure,
      otherUnitOfMesure
    } = req.body;
    const timestamp = fieldValue.serverTimestamp();
    const data1 = {
      ownerKey,
      ownerName,
      productKey: userId,
      productName: name,
      productDescription: description,
      imagePath,
      price,
      color,
      material,
      categoryName,
      subcategoryName,
      itemType,
      size,
      grade,
      unitOfMeasure,
      otherUnitOfMesure,
      isActive: true,
      updatedAt: timestamp
    }
    const data = {
      key: userId,
      name,
      categoryKey,
      categoryName,
      subcategoryName,
      itemType,
      otherItemType,
      imagePath,
      description,
      material,
      color,
      stockQuantity,
      price,
      currency,
      ownerKey,
      ownerName,
      searchCount,
      size,
      grade,
      unitOfMeasure,
      otherUnitOfMesure,
      updatedAt: timestamp
    }
    const products = db.collection('products').doc(userId).set(data, { merge: true });
    const invetory = db.collection('user.inventory').doc(userId).set(data1, { merge: true });
    //const user_inventory = db.collection('user').collection('inventory').set(data1, { merge: true});
    res.json({
      message: "Products Updated Successfully"
    })
  } catch (error) {
    res.status(500).send(error);
  }
}

