const admin = require("firebase-admin");
const db = admin.firestore();

const fieldValue = admin.firestore.FieldValue;

/** ########### FETCH USER ########### */
function fetchuserlist() {
  return new Promise(function(resolve, reject){
    setTimeout(function(){
      const userRecord = db.collection('user');
      userRecord.get().then((querySnapshot) => {
        const tempDoc = []
        querySnapshot.forEach((doc) => {
           tempDoc.push({ id: doc.id, ...doc.data() })
        })
        const error = false;
        if(!error){
          const data = JSON.stringify(tempDoc);
          //console.log(tempDoc);
          return resolve(data);
        }
        else{
          reject("Data not fetched");
        }
     })
    }, 1000);
  })
}

/** ############ GET ALL USER LIST ################ */
exports.getUserList = async(req, res) => {
  try {
    const list = await fetchuserlist();
    const data = JSON.parse(list);
    return res.status(200).send(data);
  } catch (error) {
    res.status(500).send(error); 
  }
}
//Get user by Id 
exports.getUserById = (req, res) => {
  try {
    const userId = req.params.id;
    admin
    .auth()
    .getUser(userId)
    .then((userRecord) => {
      // See the UserRecord reference doc for the contents of userRecord.
      console.log(`Successfully fetched user data: ${userRecord.email}`);
      res.json({userRecord});
      //console.log("User Data: ", req.userData);
    })
    .catch((error) => {
      console.log('Error fetching user data:', error);
    });
  } catch (error) {
    res.status(500).send(error); 
  }
}

/* ############## ADD NEW USER ############## */
exports.addNewUser = (req, res) => {
  try {
   // let intrestedItems = [];
   // let intrestedCategories = [];
    const {
      uid,
      displayName, 
      email, 
      password,
      phoneNumber,
      city,
      state,
      businessName,
      GSTNumber,
      PANNumber,
      intrestedItems,
      intrestedCategories 
    } = req.body;
    admin
    .auth()
    .createUser({
      uid: uid,
      displayName,
      email: email,
      emailVerified: false,
      password: password,
      phoneNumber:phoneNumber,
      city:city,
      state:state,
      businessName:businessName,
      GSTNumber:GSTNumber,
      PANNumber:PANNumber,
      intrestedItems, 
      intrestedCategories,
      disabled: false,
      isActive:true
    })
    .then((userRecord) => {
      console.log('Successfully created new user:', userRecord.uid);
      const timestamp = fieldValue.serverTimestamp();
      const users = {
        key:`${userRecord.uid}`,
        displayName,
        email,
        phoneNumber,
        city,
        state,
        businessName,
        GSTNumber,
        PANNumber,
        intrestedItems:fieldValue.arrayUnion(intrestedItems),
        intrestedCategories:fieldValue.arrayUnion(intrestedCategories),
        isActive:true,
        createdAt: timestamp,
        updatedAt: timestamp
      }
      const user = db.collection('user').doc(`${userRecord.uid}`).set(users, { merge: true});
    })
    .catch((error) => {
      console.log('Error creating new user:', error);
    });
    //console.log("user Id", uid);
    res.json({ message: "User Added Successfully"});
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
}

/* ############ UPDATE USER BY ID ############## */
exports.updateUserById = (req, res) =>{
  try {
    const userId = req.params.id;
    const {
      uid,
      userName, 
      email, 
      password,
      phoneNumber,
      city,
      state,
      businessName,
      GSTNumber,
      PANNumber,
      intrestedItems =[],
      intrestedCategories=[]
    } = req.body;
    admin
    .auth()
    .updateUser(userId, {
      uid: uid,
      displayName: userName,
      email: email,
      emailVerified: true,
      password: password,
      phoneNumber:phoneNumber,
      city:city,
      state:state,
      businessName:businessName,
      GSTNumber:GSTNumber,
      PANNumber:PANNumber,
      intrestedItems:intrestedItems,
      intrestedCategories:intrestedCategories,
      disabled: false,
      isActive:true
    })
    .then((userRecord) => {
      console.log(`Successfully updated user:, ${userRecord.uid}`);
      const timestamp = fieldValue.serverTimestamp();
      const users = {
        key:`${userRecord.uid}`,
        displayName:userName,
        email,
        password,
        phoneNumber,
        city,
        state,
        businessName,
        GSTNumber,
        PANNumber,
        intrestedItems:intrestedItems,
        intrestedCategories:intrestedCategories,
        isActive:true,
        createdAt:timestamp,
        updatedAt: timestamp
      }
      db.collection('user').doc(`${userRecord.uid}`).set(users, { merge: true});
      
      //db.collection('user').doc(`${userRecord.uid}`).update({ updatedAt: timestamp });
    })
    .catch((error) => {
      console.log('Error updating user:', error);
    });
    res.json({ message: "User updated Successfully"});
  } catch (error) {
    res.status(500).send(error);
  }
}

/* ################# DELETE USER BY ID ################ */
exports.deleteUserById = (req, res) => {
  try {
    const userId =req.params.id;
    admin
    .auth()
    .deleteUser(userId)
    .then(() => {
      console.log('Successfully deleted user');
      db.collection("user").doc(userId).delete();
    })
    .catch((error) => {
      console.log('Error deleting user:', error);
    });
    res.json({
      id:userId,
    })
  } catch (error) {
    res.status(500).send(error);
  }
}