const express = require('express');
const router = express.Router();

const User = require("./user");
const product =require("./product");
const search = require("./search_history");
const category = require("./categories");
const token = require("./login_token");
const auth_middelware = require("../middelware/auth_middelware");

//login token route 
router.post('/login', token.login); 

//user routes
router.get('/:id', auth_middelware, User.getUserById);
router.get('/', auth_middelware, User.getUserList);
router.post('/', auth_middelware, User.addNewUser);
router.put('/:id', auth_middelware, User.updateUserById);
router.delete("/:id", auth_middelware, User.deleteUserById);

//products routes
router.post('/products', auth_middelware, product.addProducts);
router.put('/products/:id', auth_middelware, product.updateProducts);
router.post('/products/upload', auth_middelware, product.upload_image);

//search history routes
router.post('/search', auth_middelware, search.searchProduct);

//categories routes
router.post('/categories', auth_middelware, category.addCategories);
router.put('/categories/:id', auth_middelware, category.updateCategories);
module.exports = router;  